Nibbler
=======

This project is a proof of concept of porting functionality from the [Poppler](https://poppler.freedesktop.org) library to [Rust](https://www.rust-lang.org). The long-term goal is to be able to reimplement Poppler's frontend libraries using this library, either directly in Rust for the GLib frontend or using C++ and this library's C bindings for the Qt frontend.
