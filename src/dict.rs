/*

Copyright 2018 Adam Reichold

This file is part of Nibbler.

Nibbler is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Nibbler is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Nibbler. If not, see <http://www.gnu.org/licenses/>.

*/
use std::collections::HashMap;

use nom::multispace0;

use super::object::{parse_name, parse_object, Object};

#[derive(Clone, Debug)]
pub struct Dict(HashMap<String, Object>);

impl Dict {
    pub fn get<'a>(&'a self, name: &str) -> Option<&'a Object> {
        self.0.get(name)
    }

    pub fn has_type(&self, type_: &str) -> bool {
        if let Some(Object::Name(name)) = self.0.get("Type") {
            return name == type_;
        }

        false
    }
}

named!(pub parse_dict<&[u8], Dict>,
    do_parse!(
        tag!(b"<<") >> multispace0 >>
        entries: fold_many0!(parse_entry, HashMap::new(), |mut entries: HashMap<_, _>, entry: (_, _)| {
            entries.insert(entry.0, entry.1);

            entries
        }) >>
        tag!(b">>") >> multispace0 >>
        (
            Dict(entries)
        )
    )
);

named!(parse_entry<&[u8], (String, Object)>,
    do_parse!(
        name: parse_name >>
        object: parse_object >>
        (
            name, object
        )
    )
);
