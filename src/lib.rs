/*

Copyright 2018 Adam Reichold

This file is part of Nibbler.

Nibbler is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Nibbler is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Nibbler. If not, see <http://www.gnu.org/licenses/>.

*/
#![recursion_limit = "1024"]

#[macro_use]
extern crate error_chain;
extern crate flate2;
#[macro_use]
extern crate if_chain;
#[macro_use]
extern crate log;
extern crate memmap;
#[macro_use]
extern crate nom;
extern crate twoway;

mod array;
mod catalog;
mod dict;
mod document;
mod errors;
pub mod ffi;
mod object;
mod page;
mod parser;
mod stream;
mod tiny_set;
mod xref;

pub use self::document::Document;
pub use self::errors::*;
pub use self::page::Page;
