/*

Copyright 2018 Adam Reichold

This file is part of Nibbler.

Nibbler is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Nibbler is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Nibbler. If not, see <http://www.gnu.org/licenses/>.

*/
use std::fs::File;
use std::path::Path;

use memmap::Mmap;
use nom::{is_digit, multispace1};

use super::catalog::Catalog;
use super::errors::{into_parser_result, ErrorKind, Result, ResultExt};
use super::page::Page;
use super::xref::{find_xref_table, XRefContext, XRefTable};

pub struct Document {
    input: Mmap,
    version: (u8, u8),
    xref_table: XRefTable,
    catalog: Catalog,
}

impl Document {
    pub fn new<P: AsRef<Path>>(path: P) -> Result<Self> {
        let file = File::open(path)?;
        let input = unsafe { Mmap::map(&file)? };

        let version = into_parser_result(parse_pdf_version(&input))
            .chain_err(|| ErrorKind::InvalidVersion)?;

        let (xref_table, trailer_dict) = find_xref_table(&input)?;

        let catalog = Catalog::new(XRefContext::new(&input, &xref_table), &trailer_dict)?;

        Ok(Document {
            input,
            version,
            xref_table,
            catalog,
        })
    }

    pub fn version(&self) -> (u8, u8) {
        self.version
    }

    pub fn page_count(&self) -> usize {
        self.catalog.page_count()
    }

    pub fn page(&self, index: usize) -> Result<Page> {
        let xref_context = XRefContext::new(&self.input, &self.xref_table);

        self.catalog.page(xref_context, index)
    }
}

named!(parse_pdf_version<&[u8], (u8, u8)>,
    do_parse!(
        tag!(b"%PDF-") >>
        major: parse_digit >>
        tag!(b".") >>
        minor: parse_digit >>
        multispace1 >>
        (
            major, minor
        )
    )
);

named!(parse_digit<&[u8], u8>,
    map!(verify!(take!(1), |digit: &[u8]| is_digit(digit[0])), |digit| digit[0] - b'0')
);

#[cfg(test)]
mod tests {
    #[test]
    fn parse_digit() {
        assert!(super::parse_digit(b"/").is_err());
        assert_eq!(0, super::parse_digit(b"0").unwrap().1);
        assert_eq!(9, super::parse_digit(b"9").unwrap().1);
        assert!(super::parse_digit(b":").is_err());
    }

    #[test]
    fn parse_pdf_version() {
        assert_eq!((1, 0), super::parse_pdf_version(b"%PDF-1.0\n%").unwrap().1);
    }
}
