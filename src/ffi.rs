/*

Copyright 2018 Adam Reichold

This file is part of Nibbler.

Nibbler is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Nibbler is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Nibbler. If not, see <http://www.gnu.org/licenses/>.

*/
#![allow(clippy::missing_safety_doc)]

use std::ffi::{CStr, OsStr};
use std::os::raw::c_char;
use std::os::unix::ffi::OsStrExt;
use std::ptr::null_mut;

use super::document::Document;

#[no_mangle]
pub unsafe extern "C" fn nibbler_document_new(path: *const c_char) -> *mut Document {
    let path = CStr::from_ptr(path);
    let path = OsStr::from_bytes(path.to_bytes());

    Document::new(path)
        .map(|document| Box::into_raw(Box::new(document)))
        .unwrap_or(null_mut())
}

#[no_mangle]
pub unsafe extern "C" fn nibbler_document_drop(document: *mut Document) {
    Box::from_raw(document);
}

#[no_mangle]
pub unsafe extern "C" fn nibbler_document_page_count(document: *mut Document) -> usize {
    (&*document).page_count()
}
