/*

Copyright 2018 Adam Reichold

This file is part of Nibbler.

Nibbler is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Nibbler is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Nibbler. If not, see <http://www.gnu.org/licenses/>.

*/
use nom::multispace0;

use super::object::{parse_object, Object};

#[derive(Clone, Debug)]
pub struct Array(Vec<Object>);

impl Array {
    pub fn get(&self, index: usize) -> Option<&Object> {
        self.0.get(index)
    }

    pub fn iter(&self) -> impl Iterator<Item = &Object> {
        self.0.iter()
    }
}

named!(pub parse_array<&[u8], Array>,
    do_parse!(
        tag!(b"[") >> multispace0 >>
        objects: fold_many0!(parse_object, Vec::new(), |mut objects: Vec<_>, object| {
            objects.push(object);

            objects
        }) >>
        tag!(b"]") >> multispace0 >>
        (
            Array(objects)
        )
    )
);
