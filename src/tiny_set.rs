/*

Copyright 2018 Adam Reichold

This file is part of Nibbler.

Nibbler is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Nibbler is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Nibbler. If not, see <http://www.gnu.org/licenses/>.

*/
use std::collections::hash_set::HashSet;
use std::hash::Hash;

enum Data<T: Eq + Hash> {
    Inline(u8, [T; 2]),
    Heap(HashSet<T>),
}

pub struct TinySet<T: Eq + Hash>(Data<T>);

impl<T: Default + Clone + Eq + Hash> TinySet<T> {
    pub fn new() -> Self {
        TinySet(Data::Inline(0, Default::default()))
    }

    pub fn insert(&mut self, value: T) -> bool {
        let set = match self.0 {
            Data::Inline(ref mut len, ref mut data) => {
                if data[..*len as usize].iter().any(|data| *data == value) {
                    return false;
                }

                if *len as usize != data.len() {
                    data[*len as usize] = value;
                    *len += 1;
                    return true;
                } else {
                    trace!("tiny set spilled onto heap");

                    let mut set = HashSet::new();
                    set.extend(data.iter().cloned());
                    set.insert(value);
                    set
                }
            }
            Data::Heap(ref mut set) => {
                return set.insert(value);
            }
        };

        self.0 = Data::Heap(set);
        true
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn insert_inline() {
        let mut set = TinySet::new();

        assert_eq!(true, set.insert(0));
        assert_eq!(true, set.insert(1));

        assert_eq!(false, set.insert(0));
        assert_eq!(false, set.insert(1));
    }

    #[test]
    fn insert_heap() {
        let mut set = TinySet::new();

        assert_eq!(true, set.insert(0));
        assert_eq!(true, set.insert(1));
        assert_eq!(true, set.insert(2));
        assert_eq!(true, set.insert(4));

        assert_eq!(false, set.insert(0));
        assert_eq!(false, set.insert(1));
        assert_eq!(false, set.insert(2));
        assert_eq!(false, set.insert(4));
    }
}
