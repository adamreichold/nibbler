/*

Copyright 2018 Adam Reichold

This file is part of Nibbler.

Nibbler is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Nibbler is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Nibbler. If not, see <http://www.gnu.org/licenses/>.

*/
use std::io::Read;

use super::errors::Result;
use super::stream::Stream;
use super::xref::XRefContext;

pub struct Page<'a> {
    xref_context: XRefContext<'a>,
    contents: Stream<'a>,
}

impl<'a> Page<'a> {
    pub fn new(xref_context: XRefContext<'a>, contents: Stream<'a>) -> Self {
        Self {
            xref_context,
            contents,
        }
    }

    pub fn read_contents(&self) -> Result<Box<dyn Read + 'a>> {
        self.contents.read(self.xref_context)
    }
}
