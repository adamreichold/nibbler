/*

Copyright 2018 Adam Reichold

This file is part of Nibbler.

Nibbler is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Nibbler is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Nibbler. If not, see <http://www.gnu.org/licenses/>.

*/
use std::collections::hash_map::{Entry, HashMap};

use nom::{multispace0, multispace1};
use twoway::rfind_bytes;

use super::dict::{parse_dict, Dict};
use super::errors::{into_parser_result, ErrorKind, Result};
use super::object::{parse_object, Object};
use super::parser::{parse_u32, parse_u64};
use super::stream::{parse_stream, Stream};
use super::tiny_set::TinySet;

#[derive(Default)]
pub struct XRefTable(HashMap<(u32, u32), u64>);

#[derive(Clone, Copy)]
pub struct XRefContext<'a> {
    input: &'a [u8],
    xref_table: &'a XRefTable,
}

impl<'a> XRefContext<'a> {
    pub fn new(input: &'a [u8], xref_table: &'a XRefTable) -> Self {
        Self { input, xref_table }
    }

    pub fn fetch(&self, num: u32, gen: u32) -> Result<(Object, Option<Stream<'a>>)> {
        if let Some(&pos) = self.xref_table.0.get(&(num, gen)) {
            if pos > self.input.len() as u64 {
                bail!(ErrorKind::XRefPosOutOfBounds(pos));
            }

            return into_parser_result(parse_indirect_object(&self.input[pos as usize..], *self));
        }

        bail!(ErrorKind::XRefNotFound(num, gen));
    }
}

pub fn find_xref_table(input: &[u8]) -> Result<(XRefTable, Dict)> {
    if let Some(pos) = rfind_bytes(input, b"startxref") {
        let pos = into_parser_result(parse_start_xref(&input[pos..]))?;
        return parse_xref_table_chain(input, pos, &mut TinySet::new(), Default::default());
    }

    bail!(ErrorKind::StartXRefNotFound);
}

fn parse_xref_table_chain(
    input: &[u8],
    pos: u64,
    seen_pos: &mut TinySet<u64>,
    xref_table: XRefTable,
) -> Result<(XRefTable, Dict)> {
    debug!("found xref table at position {}", pos);

    if pos > input.len() as u64 {
        bail!(ErrorKind::XRefTablePosOutOfBounds(pos));
    }

    if !seen_pos.insert(pos) {
        bail!(ErrorKind::XRefTableCircularChain(pos));
    }

    let (mut xref_table, trailer_dict) = into_parser_result(parse_xref_table_and_trailer_dict(
        &input[pos as usize..],
        xref_table,
    ))?;

    if let Some(&Object::Int(pos)) = trailer_dict.get("Prev") {
        xref_table = parse_xref_table_chain(input, pos as u64, seen_pos, xref_table)?.0;
    }

    Ok((xref_table, trailer_dict))
}

named!(parse_start_xref<&[u8], u64>,
    do_parse!(
        tag!(b"startxref") >> multispace1 >>
        pos: terminated!(parse_u64, multispace1) >>
        (
            pos
        )
    )
);

named_args!(parse_xref_table_and_trailer_dict<'a>(xref_table: XRefTable)<&'a [u8], (XRefTable, Dict)>,
    do_parse!(
        xref_table: apply!(parse_xref_table, xref_table) >>
        tag!(b"trailer") >> multispace1 >>
        trailer_dict: parse_dict >>
        (
            xref_table, trailer_dict
        )
    )
);

named_args!(parse_xref_table<'a>(xref_table: XRefTable)<&'a [u8], XRefTable>,
    do_parse!(
        tag!(b"xref") >> multispace1 >>
        num: terminated!(parse_u32, multispace1) >>
        terminated!(parse_u32, multispace1) >>
        num_and_xref_table: fold_many0!(parse_xref, (num, xref_table), |(num, mut xref_table): (u32, XRefTable), (gen, pos)| {
            trace!("parsed xref for {}@{} with position {}", num, gen, pos);

            if let Entry::Vacant(entry) = xref_table.0.entry((num, gen)) {
                entry.insert(pos);
            }

            (num + 1, xref_table)
        }) >>
        (
            num_and_xref_table.1
        )
    )
);

named!(parse_xref<&[u8], (u32, u64)>,
    do_parse!(
        pos: terminated!(parse_u64, multispace1) >>
        gen: terminated!(parse_u32, multispace1) >>
        one_of!(b"fn") >> multispace1 >>
        (
            gen, pos
        )
    )
);

named_args!(parse_indirect_object<'a>(xref_context: XRefContext)<&'a [u8], (Object, Option<Stream<'a>>)>,
    do_parse!(
        num: terminated!(parse_u32, multispace1) >>
        gen: terminated!(parse_u32, multispace1) >>
        tag!(b"obj") >> multispace0 >>
        object: parse_object >>
        stream: opt!(apply!(parse_stream, xref_context, &object)) >>
        tag!(b"endobj") >> multispace0 >>
        ({
            trace!("parsed indirect object {}@{}", num, gen);

            (object, stream)
        })
    )
);
