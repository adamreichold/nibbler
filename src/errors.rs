/*

Copyright 2018 Adam Reichold

This file is part of Nibbler.

Nibbler is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Nibbler is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Nibbler. If not, see <http://www.gnu.org/licenses/>.

*/
use nom::{Context, Err, IResult};

use super::object::Object;

error_chain! {
    errors {
        Parser(context: Option<String>) {}
        InvalidVersion {}
        StartXRefNotFound {}
        XRefTablePosOutOfBounds(pos: u64) {}
        XRefTableCircularChain(pos: u64) {}
        XRefPosOutOfBounds(pos: u64) {}
        XRefNotFound(num: u32, gen: u32) {}
        XRefCircularChain(num: u32, gen: u32) {}
        InvalidCatalog {}
        InvalidPage {}
        InvalidFilter(object: Object) {}
        UnknownFilter(filter: String) {}
        PredictorInvalidParameters {}
        PredictorParameterOverflow {}
    }

    foreign_links {
        Io(::std::io::Error);
    }
}

pub fn into_parser_result<'a, T>(res: IResult<&'a [u8], T>) -> Result<T> {
    res.map(|(_, val)| val).map_err(|err| match err {
        Err::Error(Context::Code(context, _)) | Err::Failure(Context::Code(context, _)) => {
            let context = if context.len() > 32 {
                &context[..32]
            } else {
                context
            };

            ErrorKind::Parser(Some(String::from_utf8_lossy(context).into_owned())).into()
        }
        _ => ErrorKind::Parser(None).into(),
    })
}
