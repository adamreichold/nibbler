/*

Copyright 2018 Adam Reichold

This file is part of Nibbler.

Nibbler is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Nibbler is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Nibbler. If not, see <http://www.gnu.org/licenses/>.

*/
use nom::{multispace0, multispace1, Err, IResult, Needed};

use super::array::{parse_array, Array};
use super::dict::{parse_dict, Dict};
use super::errors::{ErrorKind, Result};
use super::parser::{is_delimiter, is_multispace, parse_f64, parse_i64, parse_u32};
use super::stream::Stream;
use super::tiny_set::TinySet;
use super::xref::XRefContext;

#[derive(Clone, Debug)]
pub enum Object {
    Name(String),
    Ref(u32, u32),
    Bool(bool),
    Int(i64),
    Float(f64),
    String(String),
    Array(Array),
    Dict(Dict),
    Bytes(Vec<u8>),
}

impl Object {
    pub fn int(&self) -> Option<i64> {
        match self {
            Object::Int(int) => Some(*int),
            _ => None,
        }
    }

    pub fn array(&self) -> Option<&Array> {
        match self {
            Object::Array(array) => Some(array),
            _ => None,
        }
    }

    pub fn dict(&self) -> Option<&Dict> {
        match self {
            Object::Dict(dict) => Some(dict),
            _ => None,
        }
    }

    pub fn fetch<'a>(&self, xref_context: XRefContext<'a>) -> Result<(Self, Option<Stream<'a>>)> {
        let mut object_and_stream = (self.clone(), None);

        let mut seen_ref = TinySet::new();

        while let (Object::Ref(num, gen), _) = object_and_stream {
            if !seen_ref.insert((num, gen)) {
                bail!(ErrorKind::XRefCircularChain(num, gen));
            }

            object_and_stream = xref_context.fetch(num, gen)?;
        }

        Ok(object_and_stream)
    }

    pub fn fetch_object(&self, xref_context: XRefContext) -> Result<Self> {
        let (object, _) = self.fetch(xref_context)?;

        Ok(object)
    }

    pub fn fetch_optional_object(
        object: Option<&Self>,
        xref_context: XRefContext,
    ) -> Result<Option<Self>> {
        if let Some(object) = object {
            let object = object.fetch_object(xref_context)?;

            Ok(Some(object))
        } else {
            Ok(None)
        }
    }
}

named!(pub parse_object<&[u8], Object>,
    do_parse!(
        object: alt!(
            map!(parse_name, Object::Name) |
            parse_ref |
            parse_bool |
            parse_int |
            parse_float |
            parse_string |
            map!(parse_array, Object::Array) |
            map!(parse_dict, Object::Dict) |
            parse_bytes
        ) >>
        ({
            trace!("parsed object: {:?}", object);

            object
        })
    )
);

named!(pub parse_name<&[u8], String>,
    map_res!(
        terminated!(preceded!(tag!(b"/"), take_while1!(|input| !is_multispace(input) && !is_delimiter(input))), multispace0),
        |name: &[u8]| String::from_utf8(name.to_owned())
    )
);

named!(parse_ref<&[u8], Object>,
    do_parse!(
        num: parse_u32 >> multispace1 >>
        gen: parse_u32 >> multispace1 >>
        tag!(b"R") >> multispace0 >>
        (
            Object::Ref(num, gen)
        )
    )
);

named!(parse_bool<&[u8], Object>,
    map!(
        terminated!(
            alt!(
                value!(true, tag!(b"true")) |
                value!(false, tag!(b"false"))
            ),
            multispace0
        ),
        Object::Bool
    )
);

named!(parse_int<&[u8], Object>,
    map!(terminated!(parse_i64, multispace0), Object::Int)
);

named!(parse_float<&[u8], Object>,
    map!(terminated!(parse_f64, multispace0), Object::Float)
);

named!(parse_string<&[u8], Object>,
    do_parse!(
        tag!(b"(") >>
        string: map_res!(
            parse_parens,
            |bytes: &[u8]| String::from_utf8(bytes.to_owned())
        ) >>
        tag!(b")") >> multispace0 >>
        (
            Object::String(string)
        )
    )
);

fn parse_parens(input: &[u8]) -> IResult<&[u8], &[u8]> {
    let mut parens = 0;

    for (pos, &val) in input.iter().enumerate() {
        if val == 0x28 {
            parens += 1;
        } else if val == 0x29 {
            if parens == 0 {
                let (output, rest) = input.split_at(pos);
                return Ok((rest, output));
            } else {
                parens -= 1;
            }
        }
    }

    Err(Err::Incomplete(Needed::Unknown))
}

named!(parse_bytes<&[u8], Object>,
    do_parse!(
        tag!(b"<") >>
        bytes: map_opt!(
            take_until!(&b">"[..]),
            unhex_bytes
        ) >>
        tag!(b">") >> multispace0 >>
        (
            Object::Bytes(bytes)
        )
    )
);

fn unhex_bytes(input: &[u8]) -> Option<Vec<u8>> {
    let mut bytes = Vec::with_capacity(input.len() / 2);

    let mut input = input.iter();

    while let (Some(&upper), Some(&lower)) = (input.next(), input.next()) {
        if let (Some(upper), Some(lower)) = (unhex_byte(upper), unhex_byte(lower)) {
            bytes.push(upper << 4 | lower)
        } else {
            return None;
        }
    }

    if input.next().is_some() {
        return None;
    }

    Some(bytes)
}

fn unhex_byte(input: u8) -> Option<u8> {
    if input >= b'0' && input <= b'9' {
        Some(input - b'0')
    } else if input >= b'A' && input <= b'F' {
        Some(input - b'A' + 10)
    } else if input >= b'a' && input <= b'f' {
        Some(input - b'a' + 10)
    } else {
        None
    }
}
