/*

Copyright 2018 Adam Reichold

This file is part of Nibbler.

Nibbler is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Nibbler is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Nibbler. If not, see <http://www.gnu.org/licenses/>.

*/
use std::io::Read;

use super::super::dict::Dict;
use super::super::errors::{ErrorKind, Result};
use super::super::object::Object;
use super::super::xref::XRefContext;

pub fn make_predictor<'a>(
    mut reader: Box<dyn Read + 'a>,
    params: Option<&Dict>,
    xref_context: XRefContext,
) -> Result<Box<dyn Read + 'a>> {
    if let Some(params) = params {
        let predictor = Object::fetch_optional_object(params.get("Predictor"), xref_context)?
            .as_ref()
            .and_then(Object::int)
            .unwrap_or(1);

        if predictor == 2 || predictor >= 10 {
            let columns = Object::fetch_optional_object(params.get("Columns"), xref_context)?
                .as_ref()
                .and_then(Object::int)
                .unwrap_or(1);

            let colors = Object::fetch_optional_object(params.get("Colors"), xref_context)?
                .as_ref()
                .and_then(Object::int)
                .unwrap_or(1);

            let bits_per_component =
                Object::fetch_optional_object(params.get("BitsPerComponent"), xref_context)?
                    .as_ref()
                    .and_then(Object::int)
                    .unwrap_or(8);

            let params = Parameters::new(columns, colors, bits_per_component)?;

            if predictor == 2 {
                reader = Box::new(TiffPredictor {
                    params,
                    inner: reader,
                });
            } else if predictor >= 10 {
                reader = Box::new(PngPredictor {
                    params,
                    inner: reader,
                });
            }
        } else if predictor != 1 {
            warn!("ignoring unknown predictor {}", predictor);
        }
    }

    Ok(reader)
}

struct Parameters {
    columns: i64,
    colors: i64,
    bits_per_component: i64,
    bytes_per_pixel: i64,
    bytes_per_row: i64,
}

impl Parameters {
    pub fn new(columns: i64, colors: i64, bits_per_component: i64) -> Result<Self> {
        if columns <= 0 || colors <= 0 || bits_per_component <= 0 || bits_per_component > 16 {
            bail!(ErrorKind::PredictorInvalidParameters);
        }

        let values = columns
            .checked_mul(colors)
            .ok_or(ErrorKind::PredictorParameterOverflow)?;

        let bytes_per_pixel = (colors
            .checked_mul(bits_per_component)
            .ok_or(ErrorKind::PredictorParameterOverflow)?
            + 7)
            / 8;

        let bytes_per_row = bytes_per_pixel
            + (values
                .checked_mul(bits_per_component)
                .ok_or(ErrorKind::PredictorParameterOverflow)?
                + 7)
                / 8;

        Ok(Self {
            columns,
            colors,
            bits_per_component,
            bytes_per_pixel,
            bytes_per_row,
        })
    }
}

struct TiffPredictor<I: Read> {
    params: Parameters,
    inner: I,
}

impl<I: Read> Read for TiffPredictor<I> {
    fn read(&mut self, buf: &mut [u8]) -> ::std::io::Result<usize> {
        todo!()
    }
}

struct PngPredictor<I: Read> {
    params: Parameters,
    inner: I,
}

impl<I: Read> Read for PngPredictor<I> {
    fn read(&mut self, buf: &mut [u8]) -> ::std::io::Result<usize> {
        todo!()
    }
}
