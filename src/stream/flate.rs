/*

Copyright 2018 Adam Reichold

This file is part of Nibbler.

Nibbler is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Nibbler is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Nibbler. If not, see <http://www.gnu.org/licenses/>.

*/
use std::io::Read;

use flate2::read::ZlibDecoder;

use super::super::errors::Result;

pub struct Flate<I: Read> {
    inner: ZlibDecoder<I>,
}

impl<I: Read> Flate<I> {
    pub fn new(inner: I) -> Result<Self> {
        Ok(Self {
            inner: ZlibDecoder::new(inner),
        })
    }
}

impl<I: Read> Read for Flate<I> {
    fn read(&mut self, buf: &mut [u8]) -> ::std::io::Result<usize> {
        self.inner.read(buf)
    }
}
