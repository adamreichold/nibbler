/*

Copyright 2018 Adam Reichold

This file is part of Nibbler.

Nibbler is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Nibbler is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Nibbler. If not, see <http://www.gnu.org/licenses/>.

*/
mod flate;
mod predictor;

use std::io::{Cursor, Read};

use nom::{multispace0, multispace1, IResult};

use self::predictor::make_predictor;
use super::dict::Dict;
use super::errors::{ErrorKind, Result};
use super::object::Object;
use super::parser::custom_failure;
use super::xref::XRefContext;

#[derive(Debug)]
pub struct Stream<'a> {
    data: &'a [u8],
    filter: Option<Object>,
    params: Option<Object>,
}

impl<'a> Stream<'a> {
    pub fn read(&self, xref_context: XRefContext) -> Result<Box<dyn Read + 'a>> {
        let mut reader: Box<dyn Read + 'a> = Box::new(Cursor::new(self.data));

        let filter = Object::fetch_optional_object(self.filter.as_ref(), xref_context)?;
        let params = Object::fetch_optional_object(self.params.as_ref(), xref_context)?;

        match filter {
            Some(Object::Name(filter)) => {
                let params = params.as_ref().and_then(Object::dict);

                reader = make_reader(reader, &filter, params, xref_context)?;
            }
            Some(Object::Array(filters)) => {
                let params = params.as_ref().and_then(Object::array);

                for (index, filter) in filters.iter().enumerate() {
                    if let Object::Name(filter) = filter.fetch_object(xref_context)? {
                        let params = params
                            .and_then(|params| params.get(index))
                            .and_then(Object::dict);

                        reader = make_reader(reader, &filter, params, xref_context)?;
                    } else {
                        bail!(ErrorKind::InvalidFilter(filter.clone()));
                    }
                }
            }
            Some(filter) => {
                bail!(ErrorKind::InvalidFilter(filter));
            }
            None => {}
        }

        Ok(reader)
    }
}

fn make_reader<'a>(
    mut reader: Box<dyn Read + 'a>,
    filter: &str,
    params: Option<&Dict>,
    xref_context: XRefContext,
) -> Result<Box<dyn Read + 'a>> {
    match filter {
        "FlateDecode" => {
            reader = make_predictor(reader, params, xref_context)?;

            reader = Box::new(flate::Flate::new(reader)?);
        }
        _ => {
            bail!(ErrorKind::UnknownFilter(filter.to_owned()));
        }
    }

    Ok(reader)
}

named_args!(pub parse_stream<'a>(xref_context: XRefContext, object: &Object)<&'a [u8], Stream<'a>>,
    do_parse!(
        tag!(b"stream") >> multispace1 >>
        stream: apply!(parse_stream_data, xref_context, object) >> multispace0 >>
        tag!(b"endstream") >> multispace1 >>
        (
            stream
        )
    )
);

fn parse_stream_data<'a>(
    input: &'a [u8],
    xref_context: XRefContext,
    object: &Object,
) -> IResult<&'a [u8], Stream<'a>> {
    if let Object::Dict(dict) = object {
        let (data, rest) = if_chain! {
            if let Some(length) = dict.get("Length");
            if let Ok(length) = length.fetch_object(xref_context);
            if let Object::Int(length) = length;
            if length >= 0;
            let length = length as usize;
            if length <= input.len();
            then {
                input.split_at(length)
            } else {
                bail!(custom_failure(input, 0));
            }
        };

        let filter = dict.get("Filter").or_else(|| dict.get("F")).cloned();
        let params = dict.get("DecodeParams").or_else(|| dict.get("DP")).cloned();

        return Ok((
            rest,
            Stream {
                data,
                filter,
                params,
            },
        ));
    }

    Err(custom_failure(input, 0))
}
