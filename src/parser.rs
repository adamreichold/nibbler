/*

Copyright 2018 Adam Reichold

This file is part of Nibbler.

Nibbler is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Nibbler is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Nibbler. If not, see <http://www.gnu.org/licenses/>.

*/
use std::str::from_utf8_unchecked;

use nom::{digit1, recognize_float, Context, Err, ErrorKind, IResult};

pub fn custom_failure(input: &[u8], code: u32) -> Err<&[u8]> {
    Err::Failure(Context::Code(input, ErrorKind::Custom(code)))
}

pub fn is_multispace(input: u8) -> bool {
    match input {
        b' ' | b'\t' | b'\r' | b'\n' => true,
        _ => false,
    }
}

pub fn is_delimiter(input: u8) -> bool {
    match input {
        b'(' | b')' | b'<' | b'>' | b'[' | b']' | b'{' | b'}' | b'/' => true,
        _ => false,
    }
}

named!(pub parse_u32<&[u8], u32>,
    map_res!(
        digit1,
        |digits| unsafe { from_utf8_unchecked(digits).parse() }
    )
);

named!(pub parse_u64<&[u8], u64>,
    map_res!(
        digit1,
        |digits| unsafe { from_utf8_unchecked(digits).parse() }
    )
);

named!(pub parse_i64<&[u8], i64>,
    do_parse!(
        sign: parse_sign >>
        number: map_res!(
            digit1,
            |digits| unsafe { from_utf8_unchecked(digits).parse() as Result<i64, _> }
        ) >>
        (
            if sign { -number } else { number }
        )
    )
);

pub fn parse_f64(input: &[u8]) -> IResult<&[u8], f64> {
    flat_map!(input, recognize_float, parse_to!(f64))
}

named!(parse_sign<&[u8], bool>,
    map!(
        opt!(
            alt!(
                value!(false, tag!(b"+")) |
                value!(true, tag!(b"-"))
            )
        ),
        |sign| sign.unwrap_or(false)
    )
);

#[cfg(test)]
mod tests {
    #[test]
    fn parse_u32() {
        assert_eq!(42, super::parse_u32(b"42 ").unwrap().1);
    }

    #[test]
    fn parse_i64() {
        assert_eq!(42, super::parse_i64(b"42 ").unwrap().1);
        assert_eq!(-23, super::parse_i64(b"-23 ").unwrap().1);
    }
}
