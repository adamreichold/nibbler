/*

Copyright 2018 Adam Reichold

This file is part of Nibbler.

Nibbler is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Nibbler is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Nibbler. If not, see <http://www.gnu.org/licenses/>.

*/
use super::dict::Dict;
use super::errors::{ErrorKind, Result};
use super::object::Object;
use super::page::Page;
use super::xref::XRefContext;

pub struct Catalog {
    pages: Vec<(u32, u32)>,
}

impl Catalog {
    pub fn new(xref_context: XRefContext, trailer_dict: &Dict) -> Result<Catalog> {
        if_chain! {
            if let Some(&Object::Ref(num, gen)) = trailer_dict.get("Root");
            if let (Object::Dict(catalog), _) = xref_context.fetch(num, gen)?;
            if catalog.has_type("Catalog");
            if let Some(&Object::Ref(num, gen)) = catalog.get("Pages");
            if let (Object::Dict(pages), _) = xref_context.fetch(num, gen)?;
            if pages.has_type("Pages");
            if let Some(Object::Array(pages)) = pages.get("Kids");
            then {
                let pages = pages
                    .iter()
                    .filter_map(|page| {
                        if let Object::Ref(num, gen) = *page {
                            Some((num, gen))
                        } else {
                            warn!("skipping invalid page ref: {:?}", page);

                            None
                        }
                    })
                    .collect();

                return Ok(Catalog { pages });
            }
        }

        bail!(ErrorKind::InvalidCatalog);
    }

    pub fn page_count(&self) -> usize {
        self.pages.len()
    }

    pub fn page<'a>(&self, xref_context: XRefContext<'a>, index: usize) -> Result<Page<'a>> {
        let (num, gen) = self.pages[index];

        if_chain! {
            if let (Object::Dict(page), _) = xref_context.fetch(num, gen)?;
            if page.has_type("Page");
            if let Some(&Object::Ref(num, gen)) = page.get("Contents");
            if let (_, Some(contents)) = xref_context.fetch(num, gen)?;
            then {
                return Ok(Page::new(xref_context, contents));
            }
        }

        bail!(ErrorKind::InvalidPage)
    }
}
