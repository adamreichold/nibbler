/*

Copyright 2018 Adam Reichold

This file is part of Nibbler.

Nibbler is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Nibbler is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Nibbler. If not, see <http://www.gnu.org/licenses/>.

*/
extern crate env_logger;
extern crate nibbler;

use std::env::args;
use std::io::Read;

use nibbler::{Document, Result};

fn main() -> Result<()> {
    env_logger::init();

    let mut args = args();
    let path = args.nth(1).unwrap();

    let document = Document::new(&path)?;

    println!(
        "PDF version: {}.{}",
        document.version().0,
        document.version().1
    );
    println!("Number of pages: {}", document.page_count());
    println!();

    for index in 0..document.page_count() {
        let page = document.page(index)?;
        let mut contents = page.read_contents()?;

        let mut buf = String::new();
        contents.read_to_string(&mut buf)?;

        println!("Contents of page {}:", index);
        println!("{}", buf);
        println!();
    }

    Ok(())
}
